undertime (1.5.0) unstable; urgency=medium

  * ship a new program: moonphases, to show moon phases (new, first, full
    third) for any given period, that I do not know where else to put.
  * display parsed date in undertime to allow user to confirm it was
    correctly parsed

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 30 Nov 2018 12:45:03 -0500

undertime (1.4.0) unstable; urgency=medium

  [ Marius Gedminas ]
  * Fix typo in README.rst

  [ Antoine Beaupré ]
  * tweaks to the release process
  * comply with the NO_COLOR informal standard
  * mention tzdiff

 -- Antoine Beaupré <anarcat@debian.org>  Sun, 23 Sep 2018 12:08:45 -0400

undertime (1.3.0) unstable; urgency=medium

  * do not output colors outside of terminals (Closes: #891381)
  * improve introduction in README
  * cross-ref contributing and code pages
  * add "say thanks" badge
  * fix crash on failed zone search introduced in 1.2
  * make warnings look a little nicer

 -- Antoine Beaupré <anarcat@debian.org>  Mon, 12 Mar 2018 13:28:02 -0400

undertime (1.2.0) unstable; urgency=medium

  * use DoubleTable instead of vt100 escape sequences (Closes: #891381)
  * outline with * and _ in black-and-white
  * ship .rst docs with package
  * add manpage (Closes: #891379)
  * expand zone guessing heuristics to cover spaces and uppercase
  * add Vcs-* headers
  * correctly mark as Arch: all
  * add gbp config for native package

 -- Antoine Beaupré <anarcat@debian.org>  Mon, 26 Feb 2018 10:34:34 -0500

undertime (1.1.0) unstable; urgency=low

  * drop --version flag that adds 370ms performance penalty
  * properly install as suffix-less `undertime` binary

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 23 Feb 2018 21:13:13 +0000

