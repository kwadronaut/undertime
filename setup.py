#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import io
import os
import os.path
import re
import sys

sys.path.insert(0, os.path.dirname(__file__))

import undertime  # noqa


mod = undertime

# for possible values see
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
classifiers = [
    'Development Status :: 5 - Production/Stable',
    'Environment :: Console',
    'Intended Audience :: End Users/Desktop',
    'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',  # noqa
    'Natural Language :: English',
    'Operating System :: POSIX',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3 :: Only',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.5',
    'Topic :: Office/Business :: Scheduling',
]

requires = [
    'parsedatetime',
    'pytz',
    'termcolor',
    'terminaltables',
]

# consider find_packages for larger projects, e.g.:
# find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"])
# http://setuptools.readthedocs.io/en/latest/setuptools.html#using-find-packages
packages = []

package_data = {}


def read(*names, **kwargs):
    return io.open(
        os.path.join(os.path.dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()


def sphinx2rst(path):
    """turn a Sphinx file into simpler RST"""
    return re.sub(':[a-z]+:`~?(.*?)`',
                  r'`\1 <%s\1.html>`_' % mod.__website__,
                  read(path))


if __name__ == '__main__':
    setup(name=mod.__prog__,
          author=mod.__author__,
          author_email=mod.__email__,
          description=mod.__description__.replace('\n', ''),
          long_description=sphinx2rst('README.rst'),
          license=mod.__license_short__,
          url=mod.__website__,
          use_scm_version=True,
          scripts=['undertime', 'moonphases'],
          setup_requires=['setuptools_scm'],
          install_requires=requires,
          classifiers=classifiers,
          )
